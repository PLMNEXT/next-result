<div align="center">
  <h1>Next Result</h1>
  <p>
    <strong></strong>
  </p>
  <p>
  <!-- prettier-ignore-start -->

![MIT or Apache 2.0 licensed](https://img.shields.io/badge/license-MIT_or_Apache_2.0-blue)

<!-- prettier-ignore-end -->
  </p>

</div>


## 许可

此项目根据以下许可证之一获得许可:
- Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or [http://www.apache.org/licenses/LICENSE-2.0])
- MIT license ([LICENSE-MIT](LICENSE-MIT) or [http://opensource.org/licenses/MIT])

## 行文准则
`PLMNEXT`遵守开源社区《贡献者公约》中规定的行为守则,对`PLMNEXT/next-result`仓库的贡献是根据《贡献者公约》的条款组织的。`PLMNEXT`团队承诺进行干预，以维护该行为准则。